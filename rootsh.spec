Name:       rootsh
Summary:    Shell wrapper for auditing
Version:    1.5.3
Release:    19
License:    GPLv3+
Source0:    https://github.com/jpschewe/rootsh/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:    rootsh.logrotate
Source2:    logrotate-rootsh.sh
Patch0:     rootsh-1.5.3-open-needs-3-args.patch
Patch1:     rootsh-1.5.3_change_permissions.patch
Patch2:     rootsh-1.5.3-fix-help-doc-and-version.patch
URL:        http://sourceforge.net/projects/rootsh
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Buildrequires:  gcc

%description
Rootsh is a wrapper for shells which logs all echoed keystrokes and 
terminal output to a file and/or to syslog. Its main purpose is the 
auditing of users who need a shell with root privileges. They start 
rootsh through the sudo mechanism.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install
mkdir -p $RPM_BUILD_ROOT/var/log/rootsh
touch $RPM_BUILD_ROOT/var/log/rootsh/logrotate

# Logrotate script
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/rootsh
install -m 500 %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/logrotate-rootsh.sh

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README AUTHORS ChangeLog THANKS INSTALL COPYING
%{_bindir}/rootsh
%attr(500,root,root) %{_bindir}/logrotate-rootsh.sh
%config(noreplace) %{_sysconfdir}/logrotate.d/rootsh
/var/log/rootsh/

%files help
%{_mandir}/man1/rootsh.1.gz

%changelog
* Fri Mar 29 2024 Ge Wang <wang__ge@126.com> - 1.5.3-19
- Rebuild for next release

* Fri Mar 29 2024 Ge Wang <wang__ge@126.com> - 1.5.3-18
- Bring source file into correspondence with described in spec file

* Fri Mar 11 2022 wangkai <wangkai385@huawei.com> - 1.5.3-17
- Fix help doc and version

* Wed Jun 23 2021 yuanxin<yuanxin24@huawei.com> - 1.5.3-16
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add buildrequires gcc

* Wed Feb 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.5.3-15
- Package init

* Wed Nov 29 2017 Jianhai Luan <luanjianhai@huawei.com> - 1.5.3-15.h6
- Do not delete /var/log/rootsh/ and /varlog/rootsh/logrotate

* Fri Nov 24 2017 Jiahai Luan <luanjianhai@huawei.com> - 1.5.3-14.h5
- Modify the attribute of /bin/logrotate-rootsh.sh to 500

* Fri Nov 24 2017 Jiahai Luan <luanjianhai@huawei.com> - 1.5.3-14.h4
- Rotate /var/log/rootsh/* and limit the size of directory

* Fri Nov 24 2017 Jiahai Luan <luanjianhai@huawei.com> - 1.5.3-14.h3
- Add rootsh.back to avoid endless loop compress in logrotate

* Fri Nov 24 2017 Jiahai Luan <luanjianhai@huawei.com> - 1.5.3-14.h2
- Change rootsh log to meet the security code of Huawei

* Thu Nov 2 2017 Jianhai Luan <luanjianhai@huawei.com> - 1.5.3-14.h1
- Add the logrotate configure file to limit log size

