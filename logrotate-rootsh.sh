#!/bin/bash
DEFAULT_PATH="/var/log/rootsh/"
MaxSize=0
GSize=0

# Get Max Size in Configure File
function getmaxsize() {
    size=`grep size /etc/logrotate.d/rootsh | head -1 | awk '{print $2}'`

    unit=${size: -1}
    case $unit in
       'G' | 'g')
           size=${size:0:-1}
           size=$(($size*1024*1024))
           ;;
       'M' | 'm')
           size=${size:0:-1}
           size=$(($size*1024))
           ;;
       'K' | 'k')
           size=${size:0:-1}
           ;;
        [[:digit:]])
           ;;
        *)
           size=102400
           ;;
    esac

    MaxSize=$size
}

function getsize() {
    GSize=`du -d 1 $1 | awk '{print $1}'`
}

function logrotate_dir() {
    path=$1
    size=$2

    getsize ${DEFAULT_PATH}
    while [ $GSize -gt $size ]; do

        file=`ls -ltr ${DEFAULT_PATH} | awk '{if(NR>1){print $9}}' | head -1`

        # Do not delete the whole directory
        if [ -n "$file" -a $file != "logrotate" ]; then
        rm -rf ${DEFAULT_PATH}/$file
        else
            break
        fi

        getsize ${DEFAULT_PATH}
    done
}

function lastaction() {
    # Get Max Size in Configure File
    getmaxsize

    # Rotate /var/log/rootsh/*
    logrotate_dir ${DEFAULT_PATH} $MaxSize

    # Move rotated file to the monitor file
    if [ -f "/var/log/rootsh/logrotate.1" ]; then
        rm -f /var/log/rootsh/logrotate.1
    fi
}

if [ $# -eq 1 ]; then
    if [ $1 == "lastaction" ]; then
        lastaction
    fi
fi

